# Plant watering system - STM32
The project is based on course Embedded System Design - E7020E given at Luleå technical University.

## Purpose
To design and implement an automatic planter watering system using an STM32 using RUST with RTFM. The water plant should after a given time measure the soil moisture level using a capactive moisture sensor([link](https://www.seeedstudio.com/Grove-Capacitive-Moisture-Sensor-Corrosion-Resistant.html). Then depending on moisture level start a water pump. 
Additonal functionallity to start the pump manually can be done using a button or using Usart. 


## Limitations
Human action to full water tank.
Non optimal value for plant moisture. 

## Behaviour
Measure the moisture in the soil, if it needs water disturb it using a water pump. 

## Safety 
Destroy the plant,underflow/overflow.
Destroy the pump if the water tank is empty.

## Liveness 
Take sensor input from moisture sensore and activte pump if enough water is in the tank. 
Display something on LCD or a LED (Fill tank)


## Hardware 
[BOM file can be found here for this project.](https://docs.google.com/spreadsheets/d/1d4BUHLfI36pDphd6aOxfKf7Oi69TC_tpt9MJ5o2VE0U/edit?usp=sharing)
The schematic and layout can be found in the hardware section.

Hardware used for this project: 
* Soil moisture sensor  
* Water pump 
* Water level sensor (On/off)
* Button
* LCD 
* Rotary encoder 
* Buck converter
* STM32

## Current status 
* Schematic Done
* Layout Done
* PCB Orded
* PCB Testing --> Burned
PCB's MCU suddenly stopped working, possible it got a 5V pulse and died. Additionaly the buck don't seem to work correctly. Due to non-working mcu and Covid-19 the project was moved onto a breadboard and a stm32f4 nucelo.
* Software implementation

### Current state of the Project
  * Hardware part in progress (card) 
  * Software part  : in progress (nucleo board)
  
### Implemented Features
*  Button can manually start "pump" (Pump = LED due to delays)
* Timer reads the soil moisture value and can start "pump".
* USART communctaion can start pump and choose duty cycle and time for pump.
* Basic PWM(Small changes needed if a real pump is to be used)
* OLED screen can print hello world.

### TO DO 
  *  OLED task: Add input with string, that can displayed on screen.
  * Disable and then enable interrupt in task is broken with the current implementation. This issue leads that queing/threading will occur and will make the mcu crash after 3 "queues/interrupt" when spawning another task.
  * Possible issue with too little flash storage. Code needs to be run in --release mode for optimization purposes for now.  
  * Actually use a motor and not LED.
  * Floating sensor should block motor/PWM from starting if water tank is low.  
  

### System
This project is built using an STM32f4xx chip in mind and uses rust crates for peripheral access for such a chip.
Therefore if you intend to use this on another chip, some changes may need to be made.

### Dependencies 
This project requires some dependencies in your cargo.toml before you can run the project.
The projects cargo.toml file can be found in plant-watering-system-stm32 / Software / stm32f / Cargo.toml.
Some dependencies are:
 * ssd1306 (For OLED)
 * stm32f4xx-hal (For stm32 peripheral access)
 * cortex-m-rtfm (For RTFM App)

### launch.jason

In the launch.jason file located in plant-watering-system-stm32 / Software / stm32f / .vscode / launch.jason,
there are launch configurations which also need to be configured depending on if you are using interface/stlink.cfg or interface/stlink-v2-1.cfg.
This is system depended.

### Cargo config

The config file in cargo located in plant-watering-system-stm32 / Software / stm32f / cargo / config,
contains options to which runner you want to open a GDB session with, this will depend on your system and if you have the correct toolchain.

### Informations on how to compile and run the system for future work and improvements
To complie this code, follow the instruction on  [The Embedded Rust Book](https://rust-embedded.github.io/book/)
To edit VScode can be used or your editor of choice.

To compile in windows, download Openocd and put in your path.
In terminal:
```
openocd -f C:\OpenOCD\scripts\interface\stlink.cfg -f C:\OpenOCD\scripts\target\stm32f4x.cfg
```
To Run the main program open another terminal and navigate to: plant-watering-system-stm32\Software\stm32f
Then write
```
cargo run --features stm32f4,rtfm --release
```

To compile in Linux, dowload Openocd using your package manager.
Open a terminal in the project directory and run the following command:
```
openocd -f openocd.cfg
```
The openocd.cfg has options for using interface/stlink.cfg or interface/stlink-v2-1.cfg. This will depend on your linux distro.
Now to run the program, open a new terminal in the project directory and enter the following command:
```
cargo run --features rtfm,pac,hal --release
```
### USART
To start the usart(Windows) [RealTerm](https://sourceforge.net/projects/realterm/) can be used.
The usart should be running on 115_200 Baud rate. Set this in the Port-tab. 
The will take 2 inputs from the usart.
1) Duty cycle send in numeric value of 0-10.  
2) Time send in numeric value of 0-30.
Example: send in 4, then 10 means 40% duty cycle for 10 time units. 

To start the usart(Linux) download Moserial or another similar terminal program with your package manager.
Configure the connection to a baudrate of 115200 8N1. The input needs to be in Hex.
The input options is the same as in the Windows example.

### Program info

The program initializes alot of peripherals in the resources and init section. If you want to change some of these,
it is suggested that you read [RM0368 Reference manual](www.st.com/resource/en/reference_manual/dm00096844.pdf) to configure these to your liking.
Also this could be useful for understanding the configuration of the RTC periodic wakeup:
[AN3371 Application Note](https://www.st.com/resource/en/application_note/dm00226326-using-the-hardware-real-time-clock-rtc-and-the-tamper-management-unit-tamp-with-stm32-microcontrollers-stmicroelectronics.pdf) 

### Problems

If you run into any problems, check in the README.md in plant-watering-system-stm32 / Software / stm32f / README.md .
Under the "Trouble Shooting" section.

