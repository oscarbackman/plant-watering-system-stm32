#![deny(unsafe_code)] // Dont allow unsafe code
//#![deny(warnings)] // Dont allow warnings
#![no_main] // Dony use the Rust standard bootstrap. We provide our own
#![no_std] // Dont use the rust standard library. We are building a binary that can run on its own.

extern crate cortex_m;
extern crate cortex_m_rt;
extern crate stm32f4xx_hal as hal;

use cortex_m::{asm};
use cortex_m_semihosting::hprintln;
use panic_halt as _;
use nb::block;
use crate::hal::stm32;
use hal::{
    adc::{
        Adc,
        config::AdcConfig,
    },
    spi::{Spi, Mode, NoMiso, Phase, Polarity},
    delay::Delay,
};
use crate::hal::prelude::*;
use crate::hal::serial::{config::Config, Rx,Event, Serial, Tx};
use hal::stm32::{USART2};
use stm32f4xx_hal::gpio::GpioExt; // för att få split() och liknande från parts att fungera
use stm32f4xx_hal::rcc::RccExt;   // för att få split() och liknande från parts att fungera  
use stm32f4xx_hal::{gpio};
use cortex_m::peripheral::syst::SystClkSource;
use core::result::Result;
use ssd1306::{mode::TerminalMode, Builder};

// Type declaration for the OLED display to be used in the print_char() function
//type Dispt = ssd1306::mode::terminal::TerminalMode<ssd1306::interface::spi::SpiInterface<stm32f4xx_hal::spi::Spi<hal::stm32::SPI1, (stm32f4xx_hal::gpio::gpioa::PA5<stm32f4xx_hal::gpio::Alternate<stm32f4xx_hal::gpio::AF5>>, stm32f4xx_hal::spi::NoMiso, stm32f4xx_hal::gpio::gpioa::PA7<stm32f4xx_hal::gpio::Alternate<stm32f4xx_hal::gpio::AF5>>)>, stm32f4xx_hal::gpio::gpioa::PA9<stm32f4xx_hal::gpio::Output<stm32f4xx_hal::gpio::PushPull>>>>; 


// RTFM APP
#[rtfm::app(device = hal::stm32 , peripherals = true)]
const APP: () = {

    struct Resources {
        // late resources
        RTC:                stm32::RTC,
        ADC:                Adc<stm32::ADC1>,
        LED:                gpio::gpioa::PA10<gpio::Output<gpio::PushPull>>,
        MOIST_ADC:          gpio::gpioa::PA4<gpio::Analog>,
        TX:                 Tx<USART2>,
        RX:                 Rx<USART2>,
        EXTI:               stm32::EXTI,
        NVIC:               stm32::NVIC,
        DELAY:              hal::delay::Delay,

        ADC_FLAG:           bool,
        COUNT2:             u32,
        BUTTON_FLAG:        bool,
        ADC_moist_read:     bool,
        DISP:               ssd1306::mode::terminal::TerminalMode<ssd1306::interface::spi::SpiInterface<stm32f4xx_hal::spi::Spi<hal::stm32::SPI1, (stm32f4xx_hal::gpio::gpioa::PA5<stm32f4xx_hal::gpio::Alternate<stm32f4xx_hal::gpio::AF5>>, stm32f4xx_hal::spi::NoMiso, stm32f4xx_hal::gpio::gpioa::PA7<stm32f4xx_hal::gpio::Alternate<stm32f4xx_hal::gpio::AF5>>)>, stm32f4xx_hal::gpio::gpioa::PA9<stm32f4xx_hal::gpio::Output<stm32f4xx_hal::gpio::PushPull>>>>,                
        usart2_wait1:       bool,
        usart2_wait2:       bool,
        duty_cycle:         u32,
        pwm_time:           u32,
        
    }

    #[init]
    #[allow(unsafe_code)]
    fn init(cx: init::Context) -> init::LateResources {
        // Easy access to peripherals
        let cp = hal::stm32::CorePeripherals::take().unwrap();
        let mut syst = cx.core.SYST;
        let device = cx.device;  
        let nvic = cx.core.NVIC;
        let syst_oled = cp.SYST;

        // -------------------- GPIO and RTC CLOCK CONFIGURATION ------------------------------// 
        device.RCC.ahb1enr.modify(|_, w| w.gpioaen().set_bit()); //GPIOA clock enabled
        device.RCC.ahb1enr.modify( |_,w| w.gpiocen().set_bit()); //GPIOC clock enabled
        device.RCC.apb1enr.modify( |_,w| w.pwren().set_bit()); // Enable the power interface clock
        device.PWR.cr.modify( |_,w| w.dbp().set_bit()); // Enable access to the backup domain
        // Configure APB2ENR register, system configuration controller clock enable 
        device.RCC.apb2enr.modify( |_,w| w.syscfgen().set_bit());
        // Select clock configuration source for RTC, LSI
        device.RCC.bdcr.modify( |_,w| w.rtcsel().bits(0b10));
        // Enable RTCCLK source and LSI
        device.RCC.bdcr.modify( |_,w| w.rtcen().set_bit());
        device.RCC.csr.modify( |_,w| w.lsion().set_bit());
        while !device.RCC.csr.read().lsirdy().bit_is_set(){

        }
        device.RCC.bdcr.modify( |_,w| w.rtcen().set_bit());

        // ---------------- Auto-Wake up unit configuration ------------------------- //
        // 1. Disable the RTC register Write protection (RTC registers can now be modified)
        device.RTC.wpr.write(|w| unsafe{w.bits(0xCA)}); 
        device.RTC.wpr.write(|w| unsafe{w.bits(0x53)}); 
        
        // Clear RTC wakeup flag 
        device.RTC.isr.modify(|_,w| w.wutf().clear_bit());        
    
        // 2. Disable wakeup timer
        device.RTC.cr.modify( |_, w| w.wute().clear_bit()); 
        
        // 3.  Ensure access to Wakeup auto-reload counter and bits WUCKSEL[2:0] is allowed (Takes approx 2 RTCCLK clock cycles)
        // Poll wutwf register until its bit is set  
        let mut ready:bool = false;
        while !ready {
            if device.RTC.isr.read().wutwf().bit_is_set() == true   {
                //device.RTC.isr.modify(|_,w| unsafe {w.bits(0xFFFFFFFF)} );
                ready = true;
                hprintln!("ready").unwrap();
            }
            else {
                hprintln!("Not ready").unwrap();
            }
        }

        // 4. Program the value into the wakeup timer (Max/min wakeup period)
        device.RTC.wutr.modify( |_,w| unsafe{ w.wut().bits(0xFFFF)} ); // Set to 30 ish sec
        // Enable wake up timer interrupt
        device.RTC.cr.modify( |_,w| w.wutie().set_bit());

        // 5. Select the desired clock source
        device.RTC.cr.modify(|_,w| unsafe {w.wcksel().bits(0b000)} );
     
        // 6. Re-enable the wakeup timer. (The wakeup timer restars downcounting)
        device.RTC.cr.modify( |_,w| w.wute().set_bit());
        
        // 7. Enable the RTC registers Write protection (RTC registers can no more be modified)
        device.RTC.wpr.write(|w| unsafe{w.bits(0xFF)}); // close 
                
        // --------------- RCC,clocks and GPIO splits ------------- //
        
        // RCC constrain and freeze cfgr register
        let rcc = device.RCC.constrain();
        let clocks = rcc.cfgr.freeze();

        // Pull-up/pull-down register (Pull-up for PC13) 
        device.GPIOC.pupdr.modify( |_,w| unsafe{ w.pupdr13().bits(0b01)}); 
        device.GPIOA.pupdr.modify( |_,w| unsafe {w.pupdr1().bits(0b01)}); 
        // Split GPIOA, GPIOB and GPIOC peripherals
        let gpioa_split = device.GPIOA.split();
        let gpiob_split = device.GPIOB.split();
        let gpioc_split = device.GPIOC.split();
        
        // --------------- USART SETUP ------------------//

        // Assign pa2 and pa3 as tx, rx
        let tx = gpioa_split.pa2.into_alternate_af7();
        let rx = gpioa_split.pa3.into_alternate_af7();

        let mut serial = Serial::usart2(
            device.USART2,
            (tx,rx),
            Config::default().baudrate(115_200.bps()),
            clocks,

        )
        .unwrap();

        serial.listen(Event::Rxne);
        let (tx,rx) = serial.split();
        
        // ------------ EXTI INTERRUPT REGISTERS START -------------------- //
        
        // Connect EXTI0 to PA0 pin
        device.SYSCFG.exticr1.modify( |_,w| unsafe{w.exti0().bits(0b000)}); 
        // Connect EXTI1 to PA1 pin
        device.SYSCFG.exticr1.modify( |_,w| unsafe{w.exti1().bits(0b000)}); 
        // Enable the external interrupt for the push button on rise
        device.EXTI.imr.modify( |_,w| w.mr0().set_bit());
        // Falling edge trigger
        device.EXTI.ftsr.modify( |_,w| w.tr0().set_bit());
        // Configure mask register for exti1
        device.EXTI.imr.modify( |_,w| w.mr1().set_bit());
        device.EXTI.ftsr.write( |w| w.tr1().set_bit());
        // Enable the external interrupt for the push button on rise
        device.EXTI.imr.modify( |_,w| w.mr13().set_bit());
        // Rising edge trigger
        device.EXTI.rtsr.modify( |_,w| w.tr13().set_bit());
        // Enable exti22 interrupt
        device.EXTI.imr.modify( |_,w| w.mr22().set_bit());
        device.EXTI.emr.modify( |_,w| w.mr22().set_bit());
        device.EXTI.rtsr.modify( |_,w| w.tr22().set_bit());
        // Connect GPIOC13 User button to EXTI13 interrupt
        device.SYSCFG.exticr4.modify(|_,w| unsafe{w.exti13().bits(0b0010)});
     
        // --------------- SYSTEM TIMER CONFIGURATION START -------------// 
        syst.set_clock_source(SystClkSource::Core);
        syst.set_reload(16_000_000); // period = 1s
        syst.enable_counter();
  
        // ------------------- OLED SETUP START -------------------//
        
        let mut cs = gpiob_split.pb6.into_push_pull_output();
        let mut sck = gpioa_split.pa5.into_alternate_af5();
        let mut mosi = gpioa_split.pa7.into_alternate_af5();
        let mut dc = gpioa_split.pa9.into_push_pull_output();
        let mut res = gpioc_split.pc7.into_push_pull_output();

        let mode = Mode {
            polarity: Polarity::IdleHigh,
            phase: Phase::CaptureOnFirstTransition,
        };
        let mut spi = Spi::spi1(device.SPI1,(sck, NoMiso, mosi),mode,500_000.hz(),clocks);
        let mut delay = Delay::new(syst_oled,clocks);
        let mut disp: TerminalMode<_> = Builder::new().connect_spi(spi, dc).into();
        disp.reset(&mut res, &mut delay).unwrap();

        disp.init().unwrap();
        disp.clear().unwrap();
              
        // pass on late resources
        init::LateResources {
            RTC:         device.RTC,
            ADC:          Adc::adc1(device.ADC1,true, AdcConfig::default()),  
            LED:          gpioa_split.pa10.into_push_pull_output(),
            MOIST_ADC:    gpioa_split.pa4.into_analog(),
            DISP:         disp,
            TX:     tx,
            RX:     rx,
            EXTI:   device.EXTI,
            NVIC:   nvic,
            DELAY:   delay,

            COUNT2:             0,
            ADC_FLAG:           true,
            BUTTON_FLAG:        false,
            ADC_moist_read:     false,
            usart2_wait1:       false,
            usart2_wait2:       false,
            duty_cycle:         0,
            pwm_time:           0,
        }
    }

    // NOTICE: SHARED RESOURCES CANNOT BE ACCESSED FROM IDLE
    // Idle loop , puts the MCU in waiting for interrupt mode
    #[idle] // 
    fn idle(cx: idle::Context) ->! {
        loop {
           hprintln!("Go to sleep").unwrap(); 
           // Sleep the MCU and wait for interrupt to wakeup 
           asm::wfi();
            hprintln!("Done.....").unwrap();
        }

    }
    
    // RTC periodic interrupt that happens each 30 ish sec  
    /**
    * Task: moist_read
    * Description: RTC periodic interrupt task that happens at the time specified during init, is set in the WUTR register.
    * Inputs: None
    * Returns: None
    * Resources: RTC,EXTI
    * Spawns: moist_read() 
    */
    #[task(binds = RTC_WKUP, spawn = [moist_read] ,priority = 4, resources = [RTC,EXTI])]
    fn rtc_interrupt(cx: rtc_interrupt::Context) {

        // Resources
        let RTC = cx.resources.RTC;
        let EXTI = cx.resources.EXTI;

        // Spawn moist_read task here
        cx.spawn.moist_read().unwrap();

        // To exit and reset interrupt
        RTC.isr.modify(|_,w| w.wutf().clear_bit()); 
        EXTI.pr.modify( |_,w| w.pr22().set_bit());
            
    }

    /**
    * Task: moist_read
    * Description: Task which spawns from RTC periodic wakeup, performes measurements of the moisture sensor
    * Inputs: None
    * Returns: None
    * Resources: ADC,MOIST_ADC and ADC_moist_read
    * Spawns: pwm() 
    */
    #[task(priority = 3, resources = [ADC,MOIST_ADC,ADC_moist_read], spawn = [pwm])]
    fn moist_read(cx: moist_read::Context) {
        
        // Resources
        let MOIST_ADC = cx.resources.MOIST_ADC;
        let ADC = cx.resources.ADC;
        let ADC_moist_read: &mut bool = cx.resources.ADC_moist_read;
        *ADC_moist_read = false;
        
        // Initiate local variables
        let mut moist_val:u32 = 4000;
        let compare_val:f32 = 2000.0;
        let mut moisty: u16 = 4000;
        
        // Start soil moisture measurement
      //  while (moist_val > 2000){
        hprintln!("Starting soil measurement").unwrap();
        moist_val = 0;
        for _ in 0..1000 {
            moisty = ADC.read(MOIST_ADC).unwrap();
            moist_val = moist_val + moisty as u32;
        }

        hprintln!("Soil value: {}", (moist_val as f32 / 1000.0)).unwrap();
        
        // Check soil moisture and if soil is dry, start pwm for water pump
        if (moist_val as f32) / 1000.0 > compare_val {
            hprintln!("PWM mode started: {}",moist_val).unwrap();
            *ADC_moist_read = true;
            cx.spawn.pwm(5,5).unwrap();            
        }
        else { // if value of soil is adequate, stop pwm
            hprintln!("PWM mode stopped: {}",moist_val).unwrap();
        }
  //  }
    }

    /**
    * Task: usart_2
    * Description: 
    * Inputs: None
    * Returns: None
    * Resources: RX,TX,usart2_wait1,usart2_wait2,duty_cycle,pwm_time
    * Spawns: pwm() 
    */
    #[task(binds = USART2, priority = 3, resources = [RX,TX,usart2_wait1,usart2_wait2,duty_cycle,pwm_time], spawn = [pwm])] // Task bunden till USART2 interrupt
    fn usart2(cx: usart2::Context) {
        let rx = cx.resources.RX;
        let usart2_wait1: &mut bool = cx.resources.usart2_wait1;     //To be able to write to both duty_cycle and pwm
        let duty_cycle: &mut u32 = cx.resources.duty_cycle;
        let pwm_time: &mut u32 = cx.resources.pwm_time;
        
        if *usart2_wait1 == false{
            match block!(rx.read()) {
                Ok(byte) => {
                    *duty_cycle = byte as u32;
                    hprintln!("You choose: {}% duty cycle.",*duty_cycle*10).unwrap();
                    *usart2_wait1 = true;
                    hprintln!("How long should the pump be on [0-30]:").unwrap();
                }
                Err(err) => {
                    hprintln!("you failed due to the following error:").unwrap();
                }
            }
        }

        else {
            match block!(rx.read()) {
                Ok(byte) => {
                    *pwm_time = byte as u32;
                    hprintln!("You choose: {} time units",*pwm_time).unwrap();
                }
                Err(err) => {
                    hprintln!("you failed.").unwrap();
                }
            }
            *usart2_wait1 = false;
    

            if (*pwm_time <31) & (*duty_cycle < 11) {   //Spawns pwm if legal values been written
            cx.spawn.pwm(*pwm_time,*duty_cycle).unwrap();
            }
            else {
            hprintln!("choose value duty_cycle in range 0-10 and time in range 0-30 , pls").unwrap();
            }
        }  

    }


    /**
    * Task: pwm
    * Description: Task which outputs a customized pwm signal onto a led.
    * Inputs: time as u32 and duty_cycle as u32
    * Returns: None
    * Resources: LED,ADC_moist_read
    * Spawns: moist_read(), if it was triggered by moist_read() 
    */
    #[task(priority = 1, resources = [LED,ADC_moist_read,DELAY], spawn = [moist_read] )]
    fn pwm(mut cx: pwm::Context,time: u32, duty_cycle: u32) {
        hprintln!("IN PWM ").unwrap();
      
        let led = cx.resources.LED;
       // led.set_high();
        let led_high1 = led.set_high();
        let mut pwn_spawn_moist_read:bool = false;

        for _ in 0..time{

            let led_high = led.set_high();
            // Error handling
            match led_high{
                Result::Ok(_) => (),
                Result::Err(err) => {
                    hprintln!("Paniced at led.set.high() in PWM task, the error is: {}", err).unwrap();
                    panic!("Paniced at led.set.high() in PWM task, the error is: {}", err);
                },
            }
            cx.resources.DELAY.delay_ms(duty_cycle*100);


            let led_low = led.set_low();
            // Error handling
            match led_low{
                Result::Ok(_) => (),
                Result::Err(err) => {
                    hprintln!("Paniced at led.set.low() in PWM task, the error is {}", err).unwrap();
                    panic!("Paniced at led.set.low() in PWM task, the error is {}", err);
                },
            }
            cx.resources.DELAY.delay_ms((10-duty_cycle)*100);


        }
    }


    /**
    * Task: exti1 
    * Description: Task which handles floating sensor interrupt, would trigger LoRa task to send alert via thingsboard, now just says "danger zone" on screen
    * Inputs: None
    * Returns: None
    * Resources: EXTI
    * Spawns: OLED
    */
    #[task(binds = EXTI1, priority = 4,  resources = [EXTI],spawn = [OLED])]
    fn exti1(cx: exti1::Context) {
       hprintln!("EXIT1!").unwrap();
       cx.spawn.OLED(1).unwrap();
        cx.resources.EXTI.pr.modify( |_,w| w.pr1().set_bit()); 
    }
       /**
    * Task: OLED 
    * Description: Task That can stuff on screen, TODO: input string to function that can be printed! 
    * Inputs: None        //Could be configured to have string input to print later.
    * Returns: None
    * Resources: None
    * Spawns: None
    */
    #[task(priority = 4,resources = [DISP] )]
    fn OLED(cx: OLED::Context,state: u32 ) {
        let disp = cx.resources.DISP;

        if (state == 0){ 
        disp.print_char(' ');
        disp.print_char(' ');    
        disp.print_char('R');
        disp.print_char('U');
        disp.print_char('S');
        disp.print_char('T');
        disp.print_char('Y');
        disp.print_char(' ');
        disp.print_char('B');
        disp.print_char('U');
        disp.print_char('T');
        disp.print_char('T');
        disp.print_char('O');
        disp.print_char('N');
        disp.print_char(' ');
        disp.print_char(' ');

       }
       if (state == 1) {
        disp.print_char('D');
        disp.print_char('A');
        disp.print_char('N');
        disp.print_char('G');
        disp.print_char('E');
        disp.print_char('R');
        disp.print_char(' ');
        disp.print_char('Z');
        disp.print_char('O');
        disp.print_char('O');
        disp.print_char('O');
        disp.print_char('O');
        disp.print_char('O');
        disp.print_char('O');
        disp.print_char('N');
        disp.print_char('E');       }

    }

    /**
    * Task: exti15_10 
    * Description: Task which handles button interrupt and starts pwm
    * Inputs: None
    * Returns: None
    * Resources: EXTI,COUNT2,BUTTON_FLAG, NVIC
    * Spawns: pwm(),OLED()
    */
    #[task(binds = EXTI15_10, priority = 4 , resources = [EXTI,COUNT2,BUTTON_FLAG,NVIC],spawn = [pwm,OLED])]
    fn exti15_10(cx: exti15_10::Context) {
        cx.resources.NVIC.disable(stm32::Interrupt::EXTI15_10);
        cx.resources.NVIC.disable(stm32::Interrupt::EXTI1);

        // let dispy = cx.resources.DISP;
        //dispy.print_char('A');


        let COUNT2: &mut u32 = cx.resources.COUNT2;
        let BUTTON_FLAG: &mut bool = cx.resources.BUTTON_FLAG;
        // Critical section due to shared variable call, short as possible
        *COUNT2 += 1;
        
        cx.resources.NVIC.disable(stm32::Interrupt::EXTI15_10);
        cx.spawn.OLED(0).unwrap();

    //    if *BUTTON_FLAG == true {
            hprintln!("Button Pwm starting").unwrap();
            cx.spawn.pwm(5,5).unwrap();
     //   }
        // If it says field is pr doesnt exist in EXTI resource, the interrupts have different priorities
        cx.resources.EXTI.pr.modify( |_,w| w.pr13().set_bit()); 

        cx.resources.NVIC.disable(stm32::Interrupt::EXTI15_10);

        cx.resources.NVIC.enable(stm32::Interrupt::EXTI1);   // WHY DOES THIS NOT WORK :( 
        cx.resources.NVIC.enable(stm32::Interrupt::EXTI15_10);
    }
    extern "C"{
        fn EXTI0();
        fn EXTI4();
        fn EXTI2();

    }
};