#![no_main]
#![no_std]
//use std;
#[macro_use]


use panic_halt as _;
use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;
use cortex_m::{asm, iprintln};
use cortex_m::interrupt::{free};
use cortex_m_rt::{exception, ExceptionFrame};


use stm32f4::stm32f413::{self, DWT, GPIOA, GPIOC, RCC, ADC1, SYST};
extern crate stm32f4xx_hal as hal;
use crate::hal::{prelude::*, stm32};
use hal::{
  gpio::gpioa,
  adc::{
    Adc,
    config::AdcConfig,
    config::SampleTime,
  },
  interrupt,
};
use crate::hal::serial::{config::Config, Serial};
use nb::block;
use rtfm::app;
use core::cell::RefCell;
use cortex_m::interrupt::Mutex;
use smlang::statemachine;
use cortex_m::{iprint, peripheral::syst::SystClkSource};

#[rustfmt::skip]
mod address {
    pub const PERIPH_BASE: u32      = 0x40000000;
    pub const AHB1PERIPH_BASE: u32  = PERIPH_BASE + 0x00020000;
    pub const RCC_BASE: u32         = AHB1PERIPH_BASE + 0x3800;
    pub const RCC_AHB1ENR: u32      = RCC_BASE + 0x30;   
    pub const RCC_APB2ENR: u32      = RCC_BASE + 0x44;   
    pub const GBPIA_BASE: u32       = AHB1PERIPH_BASE + 0x0000;
    pub const GPIOA_MODER: u32      = GBPIA_BASE + 0x00;
    pub const GPIOA_PUPDR: u32      = GPIOA_MODER+0x0C;
    pub const GPIOA_BSRR: u32       = GBPIA_BASE + 0x18;
    pub const ADC_SR:u32           = 0x40012000;
    pub const ADC_CR2:u32           = 0x40012000 + 0x08;
    pub const ADC_DR:u32           = 0x40012000 + 0x4C;
}
use address::*;


statemachine! {
    *State1 + Event1 = State2,
    State2 + Event2 = State3,
}


#[inline(always)]
fn read_u32(addr: u32) -> u32 {
   unsafe { core::ptr::read_volatile(addr as *const _) }
  //  core::ptr::read_volatile(addr as *const _)
}

#[inline(always)]
fn write_u32(addr: u32, val: u32) {
    unsafe {
        core::ptr::write_volatile(addr as *mut _, val);
    }
}

#[entry]
fn main() -> ! {
  
    let dp = hal::stm32::Peripherals::take().unwrap();
//    let cortexm_peripherals = cortex_m::Peripherals::take().unwrap();
    let mut cortexm_peripherals = hal::stm32::CorePeripherals::take().unwrap();


    //let stm32f4_peripherals = &stm32f413::Peripherals::take().unwrap();
    // Set up clock.
    let mut syst = cortexm_peripherals.SYST;
    syst.set_clock_source(SystClkSource::Core);
    syst.set_reload(16_000_000); // 1s
    syst.enable_counter();
    syst.enable_interrupt();


    //let rcc = dp.RCC;
    let rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.freeze();

  //  let clocks = dp.RCC.cfgr.freeze();
   // rcc.cfgr.pclk1(16.mhz()).pclk2(16.mhz()).freeze();

        //Setting up ADC for port A2, moist_adc
    let mut adc = Adc::adc1(dp.ADC1, true, AdcConfig::default());
    let gpioa = dp.GPIOA;
    let gpioa_split = gpioa.split();
    let mut moist_adc = gpioa_split.pa4.into_analog();

    //Setting up GPIOC
    let gpioc = dp.GPIOC;
    let gpioc_split = gpioc.split();
    let gpiob = dp.GPIOB;
    let gpiob_split = gpiob.split();

    // Setting up interrupt for A0
   // rcc.ahb1enr.modify(|_, w| w.gpioaen().set_bit());  // ALSO DEFINEF ABOVE, this defined for gpioC! :/
    //rcc.apb2enr.modify(|_, w| w.syscfgen().set_bit()); // enable clock for SYSCFG
    let r = read_u32(RCC_AHB1ENR) & !(0b111 << (0)); // read and mask
    write_u32(RCC_AHB1ENR, r | 0b101 << (0)); // set output mode
    let r = read_u32(RCC_APB2ENR) & !(0b01 << (14)); // read and mask
    write_u32(RCC_APB2ENR, r | 0b1 << (14)); // set output mode

    let syscfg = dp.SYSCFG;
    let a0 = gpioa_split.pa0.into_pull_down_input();
    
    let mut led  = gpioa_split.pa5.into_push_pull_output();

    syscfg.exticr1.modify(|_, w| unsafe { w.exti0().bits(0b000) }); // connect EXTI0 to PA0 pin
    let exti = dp.EXTI;
    exti.imr.modify(|_, w| w.mr0().set_bit());   // unmask interrupt
    exti.ftsr.modify(|_, w| w.tr0().set_bit());  // trigger on rising-edge
    
    // Setting up interrupt for PC13
    //rcc.ahb1enr.modify(|_, w| w.gpiocen().set_bit());  // ALSO DEFINEF ABOVE, this defined for gpioC! :/
    let a0 = gpioc_split.pc13.into_pull_down_input();
    syscfg.exticr4.modify(|_, w| unsafe { w.exti13().bits(0b0010) }); // connect EXTI0 to PA0 pin
    exti.imr.modify(|_, w| w.mr13().set_bit());   // unmask interrupt
    exti.rtsr.modify(|_, w| w.tr13().set_bit());  // trigger on rising-edge


    // Setting up interrupt for usart2 PC9
    //rcc.ahb1enr.modify(|_, w| w.gpiocen().set_bit());  // ALSO DEFINEF ABOVE, this defined for gpioC! :/
    //let a3 = gpiob_split.pb3.into_pull_down_input();
    //syscfg.exticr2.modify(|_, w| unsafe { w.exti4().bits(0b0001) }); // connect EXTI4 to PA2 pin
    //exti.imr.modify(|_, w| w.mr3().set_bit());   // unmask interrupt
    //exti.rtsr.modify(|_, w| w.tr3().set_bit());  // trigger on rising-edge
  
    let tx = gpioa_split.pa2.into_alternate_af7();
    let rx = gpioa_split.pa3.into_alternate_af7(); // try comment out

    let serial = Serial::usart2(
      dp.USART2,
      (tx, rx),
      Config::default().baudrate(115_200.bps()),
      clocks,
  )
  .unwrap();
   
    let mut nvic = cortexm_peripherals.NVIC;
    nvic.enable(stm32::Interrupt::EXTI0);
  //  nvic.enable(stm32::Interrupt::EXTI4);
    nvic.enable(stm32::Interrupt::EXTI15_10);
    //nvic.disable(stm32::Interrupt::EXTI15_10);



    let (mut tx, mut rx) = serial.split();

       //    led.set_high();
           let old_val = 4095;
  hprintln!("HELLO WORLD").unwrap();


    loop {

if unsafe { usart2 == true } {
  hprintln!("PWM - Duty cycle [0-10]: ").unwrap();
  syst.disable_counter();
  let mut usart_pwm = 0;
  let mut usart_time = 0;

      match block!(rx.read()) {
        Ok(byte) => {
            usart_pwm = byte as u32;
            tx.write(byte).unwrap();
            hprintln!("You choose: {}",usart_pwm).unwrap();        }
        Err(err) => {
         hprintln!("you failed.").unwrap();
        }
    }
    hprintln!("How long should the pump be on [0-30]: ").unwrap();
    match block!(rx.read()) {
      Ok(byte) => {
          usart_time = byte as u32;
          tx.write(byte).unwrap();
          hprintln!("You choose: {} seconds",usart_time).unwrap();        }
      Err(err) => {
       hprintln!("you failed.").unwrap();
      }
  }
  for _ in 0..usart_time{
    led.set_low();
   // hprintln!("OFF").unwrap();          
    for _ in 0..usart_pwm*10000{
    }
    led.set_high();
   // hprintln!("ON").unwrap();            
    for _ in 0..(10-usart_pwm)*10000{
    }
  }
}


  

      let mut new_val = 0;

      // Moisture reader
        if unsafe {start_measurment == true} //Start the adc after "timer"-time.
        {
          unsafe {start_measurment = false} //Reset timer
          unsafe { timer = 0; }
          new_val = 0;
          hprintln!("Starting soil-measurment").unwrap();

          for _ in 0..1000 {                      // 1000 measurment --> mean value.
          let moisty: u16 = adc.read(&mut moist_adc).unwrap();
          new_val = new_val+moisty as u32;          // Can overflow as u16.


        }
      }
          hprintln!("soil value: {}",(new_val as f32)/1000.0).unwrap();
          if (new_val as f32)/1000.0 > 2000.0  

          {
            hprintln!("pwm mode started: {}",new_val).unwrap();

            //ica basic pwm, 
            for _ in 0..1{
              led.set_high();
              for _ in 0..100_00{
              }
              led.set_low();
              for _ in 0..100_00{
              }
            }
            unsafe {pause_mode = true}
            let old_val = new_val;
            new_val = 0; //resets
            hprintln!("pwm mode stopped: {}",old_val).unwrap();
            
            while (unsafe{pause_mode}){
              for _ in 0..20_000 {        //wait for interrupt eller dylikt här istället.
              }
            }
            hprintln!("Pause done").unwrap();
          }

          //measure val 1000 times and take mean.
          // if val >> 2000 Pump water
          // Pump water for 10s
          // Pause 1 min, read adc --> adc_new < adc_old else panic
          // pump water until happy. adc_flag == false.
        


          while(unsafe {button_flag})   // and usart == true.
          {
            hprintln!("Button mode").unwrap();
            for _ in 0..100{
              led.set_high();
              for _ in 0..100{
              }
              led.set_low();
              for _ in 0..100{
              }
            }


          }


           for _ in 0..200_000 {
           }
        continue;
    }
}
//static mut COUNT: u32 = 0;

static mut timer: u32 = 0;
static mut pause_timer: u32 = 0;
static mut start_measurment: bool  = false;
static mut pause_mode: bool  = false;

#[exception]
fn SysTick() {
    hprintln!(".").unwrap();
    unsafe { timer = timer+1; }
    if unsafe{timer > 10} {
      hprintln!("tiiimer").unwrap();
      unsafe {start_measurment = true;}

    }
    if unsafe{pause_mode == true} {
      unsafe{hprintln!("pause mode: {}",pause_mode).unwrap()};
      unsafe { timer = 0; }
      unsafe { pause_timer = pause_timer+1; }
      if unsafe {pause_timer > 3 } {
        unsafe{pause_mode = false;}
        unsafe{pause_timer = 0;}
        

      }
    }

}

static mut COUNT: u32 = 0;
static mut COUNT2: u32 = 0;
static mut adc_flag:bool = true;
static mut usart2:bool = false;
#[interrupt]
fn EXTI0() {
    hprintln!("HELLO EXTI0").unwrap();
    unsafe { usart2 = true; }
    unsafe { COUNT = COUNT+1; }
    let exti = unsafe { &*stm32f413::EXTI::ptr() }; // Better way to do this?
    exti.pr.write(|w| w.pr0().set_bit());

   // let exti = unsafe { &*stm32f413::EXTI::ptr() }; // Better way to do this?
    //exti.pr.write(|w| w.pr0().set_bit());


//*COUNT += 1;
  }


fn EXTI4() {
  hprintln!("HELLO from exti4.................................................").unwrap();
  let exti = unsafe { &*stm32f413::EXTI::ptr() }; // Better way to do this?
  exti.pr.write(|w| w.pr4().set_bit());
}

static mut button_flag: bool =  false;
#[interrupt]
fn EXTI15_10() {

 //   hprintln!("HELLO INTERRUPT").unwrap();
    //unsafe {button_flag = true};

    unsafe { COUNT2 = COUNT2+1; }
   // unsafe{hprintln!(" {}",COUNT).unwrap();}
  if unsafe{button_flag == true} {
    unsafe{button_flag = false;}
  }
  else {
    unsafe{button_flag = true;}
  }
    let exti = unsafe { &*stm32f413::EXTI::ptr() }; // Better way to do this?
    exti.pr.write(|w| w.pr13().set_bit());
   // let exti = unsafe { &*stm32f413::EXTI::ptr() }; // Better way to do this?
    //exti.pr.write(|w| w.pr0().set_bit());

//*COUNT += 1;
  }

