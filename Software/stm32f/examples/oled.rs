#![no_main]
#![no_std]
//use std;
//#[macro_use]
//extern crate lazy_static;


use panic_halt as _;
use cortex_m_rt::entry;
use cortex_m_semihosting::hprintln;
//use cortex_m::{asm, iprintln};
//use cortex_m::interrupt::{free};
//use cortex_m_rt::{exception, ExceptionFrame};


//use stm32f4::stm32f413::{self, DWT, GPIOA, GPIOC, RCC, ADC1, SYST};
extern crate stm32f4xx_hal as hal;
use crate::hal::{prelude::*, stm32};
use hal::{
  gpio::gpioa,
  delay::Delay,
  spi::{self,Spi, Mode, NoMiso, Phase, Polarity},
  adc::{
    Adc,
    config::AdcConfig,
    config::SampleTime,
  },
  interrupt,
};
use nb::block;
use rtfm::app;
use ssd1306::{mode::TerminalMode, prelude::*, Builder};



#[entry]
fn main() -> ! {
  
    let dp = hal::stm32::Peripherals::take().unwrap();
    let pp = hal::stm32::CorePeripherals::take().unwrap();
  //  let mut cortexm_peripherals = hal::stm32::CorePeripherals::take().unwrap();
    
    let rcc = dp.RCC.constrain();
    let syscfg = dp.SYSCFG;
    let clocks = rcc.cfgr.freeze();
    let gpioa = dp.GPIOA;
    let gpioa_split = gpioa.split();
    let gpiob = dp.GPIOB;
    let gpiob_split = gpiob.split();
    let gpioc = dp.GPIOC;
    let gpioc_split = gpioc.split();

    let mut cs = gpiob_split.pb6.into_push_pull_output();
    let sck = gpioa_split.pa5.into_alternate_af5();
    let mosi = gpioa_split.pa7.into_alternate_af5();

    let dc = gpioa_split.pa9.into_push_pull_output();
    let mut res = gpioc_split.pc7.into_push_pull_output();
    let mode = Mode {
      polarity: Polarity::IdleHigh,
      phase: Phase::CaptureOnFirstTransition,
  };

   // let mut spi = dp.Spi::spi1((sck, NoMiso, mosi), spi::MODE_0, 1_000_000.hz(), clocks);
   let mut spi = Spi::spi1(dp.SPI1,(sck, NoMiso, mosi),mode,500_000.hz(),clocks);
   

  // let data = spi.write(&[0x40, 0xA1, 0xC0, 0xA4, 0xA6, 0xA2, 0x2F, 0x27, 0x81, 0x10, 0xFA, 0x90, 0xAF]);
  // match data {
   //        Ok(v) => iprintln!(stim, "working with version: {:?}", v),
  //         Err(e) => iprintln!(stim, "error parsing header: {:?}", e),
   //}
   let mut syst = pp.SYST;
 //  let  test = dp
   let mut delay = Delay::new(syst,clocks);
   //let data = spi.write(&[0x40, 0xA1, 0xC0, 0xA4, 0xA6, 0xA2, 0x2F, 0x27, 0x81, 0x10, 0xFA, 0x90, 0xAF]);
  //spi.write(&[0x40, 0xA1, 0xC0, 0xA4, 0xA6, 0xA2, 0x2F, 0x27, 0x81, 0x10, 0xFA, 0x90, 0xAF]);



let mut disp: TerminalMode<_> = Builder::new().connect_spi(spi, dc).into();
disp.reset(&mut res, &mut delay).unwrap();

disp.init().unwrap();
disp.clear().unwrap();

disp.print_char('P');
disp.print_char('A');
disp.print_char(' ');
disp.print_char('M');
disp.print_char('I');
disp.print_char('N');
disp.print_char(' ');
disp.print_char('T');
disp.print_char('I');
disp.print_char('D');
disp.print_char('.');
disp.print_char('.');
disp.print_char('.');
disp.print_char('.');
for _ in 0..10000 { 
} 
  // disp.flush().unwrap();

let mut i = 0;
    hprintln!("HELLO WORLD").unwrap();
    loop {
        //continue
        //hprintln!("HELLO WORLD...................................................").unwrap();
        //spi.write(&[0xAA,0xA1, 0xC0, 0xA4, 0xA6, 0xA2, 0x2F, 0x27, 0x81, 0x10, 0xFA, 0x90, 0xAF]);
      //  disp.print_char('A');
      disp.clear().unwrap();
      disp.print_char('R');
      disp.print_char('E');
      disp.print_char('S');
      disp.print_char('E');
      disp.print_char('T');
      disp.print_char('S');
      disp.print_char(':');
      for i in 0..10{
     //   hprintln!("HELLO WORLD").unwrap();
       // hprintln!("{}",i).unwrap();
     //   let ehm = i as char;
//        disp.print_char(ehm);

         if i == 1 {
          disp.print_char('9');
        }
        if i == 2 {
          disp.set_position(7,0);
          disp.print_char('8');
        }
        if i == 3 {
          disp.set_position(7,0);
          disp.print_char('7');
        }
        if i == 4 {
          disp.set_position(7,0);
          disp.print_char('6');
        }
        if i == 5 {
          disp.set_position(7,0);
          disp.print_char('5');
        }
        if i == 6 {
          disp.set_position(7,0);
          disp.print_char('4');
        }
        if i == 7 {
          disp.set_position(7,0);
          disp.print_char('3');
        }
        if i == 8 {
          disp.set_position(7,0);
          disp.print_char('2');
        }
        if i == 9 {
          disp.set_position(7,0);
          disp.print_char('1');
        }
        for _ in 0..100000 { 

        }
      }
      
        
         
        // disp.display_on(false);
//        disp.print_char('.');
      for _ in 0..10000 { 

        } 
      }
}
