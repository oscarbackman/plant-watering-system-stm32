#![no_main]
#![no_std]
//#![deny(unsafe_code)]

extern crate panic_halt;

use cortex_m_rt::entry;
use cortex_m::{interrupt, peripheral::NVIC};
use cortex_m::interrupt::{free};
use cortex_m::interrupt::Mutex;

use stm32l0xx_hal::{pac, prelude::*, rcc::Config};
use cortex_m_semihosting::hprintln;
use stm32l0xx_hal as hal;
use stm32l0::stm32l0x1;
//use stm32l0x1::interrupt;


#[rustfmt::skip]
mod address {
    pub const PERIPH_BASE: u32      = 0x40000000;

}
#[inline(always)]
fn read_u32(addr: u32) -> u32 {
   unsafe { core::ptr::read_volatile(addr as *const _) }
  //  core::ptr::read_volatile(addr as *const _)
}
#[inline(always)]
fn write_u32(addr: u32, val: u32) {
    unsafe {
        core::ptr::write_volatile(addr as *mut _, val);
    }
}


fn EXTI2() {
  hprintln!("1510!").unwrap();
  }


#[entry]
fn main() -> ! {
    let dp = pac::Peripherals::take().unwrap();
    // Configure the clock.
   // let mut rcc12 = dp.RCC.freeze(Config::hsi16());
    let rcc = &dp.RCC;
    rcc.apb2enr.modify(|_, w| w.syscfgen().set_bit()); // enable clock for SYSCFG
    let gpioa = &dp.GPIOA;
    gpioa.moder.modify(|_, w| w.mode5().input()); // moder0 corresponds to pin 0 on GPIOA
    gpioa.pupdr.modify(|_, w| unsafe { w.pupd5().bits(0b10) }); // set mode to pull-down
    let syscfg = &dp.SYSCFG;
    syscfg.exticr1.modify(|_, w| unsafe { w.exti2().bits(0b000) }); // connect EXTI0 to PA0 pin
    let exti = &dp.EXTI;
    exti.imr.modify(|_, w| w.im11().set_bit());   // unmask interrupt
    exti.rtsr.modify(|_, w| w.rt11().set_bit());  // trigger on rising-edge
    let cortexm_peripherals = cortex_m::Peripherals::take().unwrap();
    let mut nvic = cortexm_peripherals.NVIC;
    nvic.enable(stm32l0x1::interrupt::EXTI2_3);
   // dp.exti2.nvic.
//nvic.enable(stm32f30x::Interrupt::EXTI0);


   // let r = read_u32(RCC_AHB1ENR); 
    //write_u32(RCC_AHB1ENR, r | 1); // set enable
    //let r = read_u32(RCC_AHB1ENR); 
    //write_u32(RCC_AHB2ENR, r | 0b100000000); // ENABLE ADC CLOCK!
    


    hprintln!("HELLO WORLD!").unwrap();

    loop {
        for _ in 0..200_000 {
        }
        hprintln!("test").unwrap();
  
        continue;
    }
}