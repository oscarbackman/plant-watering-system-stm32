// #![deny(warnings)]
#![deny(unsafe_code)]
#![no_main]
#![no_std]

extern crate panic_halt;

use rtfm::app;
use stm32l0xx_hal::{gpio::*, pac, prelude::*, rcc::Config, timer::Timer};
use cortex_m::{iprint, iprintln};
use cortex_m_semihosting::hprintln;

#[app(device = stm32l0xx_hal::pac)]
const APP: () = {
    static mut LED: gpioa::PA11<Input<PullUp>> = ();
   // static mut TIMER: Timer<pac::TIM2> = ();

    #[init]
    fn init() -> init::LateResources {
        // Configure the clock.
        let mut rcc = device.RCC.freeze(Config::hsi16());

        // Acquire the GPIOA peripheral. This also enables the clock for GPIOA
        // in the RCC register.
        let gpioa = device.GPIOA.split(&mut rcc);

        // Configure PA1 as output.
        let led = gpioa.pa11.into_pull_up_input();
      //  iprintln!("in init");
      hprintln!("HELLO WORLD").unwrap();



        // Return the initialised resources.
        init::LateResources {
            LED: led,
       //     TIMER: timer,
        }
    }

 
};