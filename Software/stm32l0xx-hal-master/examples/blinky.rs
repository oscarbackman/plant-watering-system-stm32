#![no_main]
#![no_std]
#![deny(unsafe_code)]

extern crate panic_halt;

use cortex_m_rt::entry;
use stm32l0xx_hal::{pac, prelude::*, rcc::Config};
use cortex_m_semihosting::hprintln;



#[entry]
fn main() -> ! {
    let dp = pac::Peripherals::take().unwrap();

    // Configure the clock.
    let mut rcc = dp.RCC.freeze(Config::hsi16());

    let mut adc   = dp.ADC.constrain(&mut rcc);
    // Acquire the GPIOA peripheral. This also enables the clock for GPIOA in
    // the RCC register.
    let gpioa = dp.GPIOA.split(&mut rcc);
    let mut button = gpioa.pa11.into_pull_down_input();
   // let val: u16 = adc.read(&mut button).unwrap();

    // Configure PA1 as output.
    let mut led = gpioa.pa5.into_push_pull_output();
    let mut led2 = gpioa.pa4.into_push_pull_output();


    hprintln!("HELLO WORLD").unwrap();

    //let val: u16 = adc.read(&mut button).unwrap();
    let mut a = button.is_low().unwrap();
  //  hprintln!("{}",a).unwrap();

    loop {
        let mut a = button.is_low().unwrap();
        let mut b = button.is_low().unwrap();
        hprintln!("{},{}",a,b).unwrap();

 //   hprintln!("{}",a).unwrap();

    if a 
    {
        hprintln!("press the knapp").unwrap();
    }
    else {
        hprintln!("You are pressing").unwrap();

     }
        
        // Set the LED high one million times in a row.
        for _ in 0..500_000 {
            led.set_high().unwrap();
            led2.set_low().unwrap();
        }
       // hprintln!("{}",button).unwrap();
        

       //  Set the LED low one million times in a row.
        for _ in 0..500_000 {
            led.set_low().unwrap();
            led2.set_high().unwrap();

        }
    }
}
