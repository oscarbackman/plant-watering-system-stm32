EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	5650 3350 11150 3350
Wire Notes Line
	11150 3350 11150 550 
Wire Notes Line
	11150 550  5650 550 
Text Notes 10200 3250 0    118  ~ 0
CAN/ADC
$Comp
L Analog_ADC:MCP3002 U5
U 1 1 5E3C604A
P 7450 1850
F 0 "U5" H 7550 1169 50  0000 C CNN
F 1 "MCP3002" H 7550 1260 50  0000 C CNN
F 2 "Package_SO:SOIC-8_3.9x4.9mm_P1.27mm" H 7450 1750 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21294E.pdf" H 7450 2050 50  0001 C CNN
	1    7450 1850
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 2050 7950 2050
Wire Wire Line
	8150 1650 7950 1650
Wire Notes Line
	5650 550  5650 3350
Wire Wire Line
	6750 1750 6400 1750
Wire Wire Line
	6750 1950 6400 1950
Wire Wire Line
	6750 2050 6400 2050
Wire Wire Line
	7450 2350 7450 2500
Wire Wire Line
	7450 1350 7450 1150
Text HLabel 8150 1650 2    50   Input ~ 0
Analog_Input_1
Text HLabel 8150 2050 2    50   Input ~ 0
Analog_input_0
Text HLabel 6400 1650 0    50   Input ~ 0
Serial_Data_Input
Wire Wire Line
	6750 1650 6400 1650
Text HLabel 6400 1750 0    50   Input ~ 0
Serial_Data_Output
Text HLabel 6400 1950 0    50   Input ~ 0
CLK
Text HLabel 6400 2050 0    50   Input ~ 0
Enable_Conversion
$Comp
L power:GND #PWR0103
U 1 1 5E3FE7E4
P 7450 2500
F 0 "#PWR0103" H 7450 2250 50  0001 C CNN
F 1 "GND" H 7455 2327 50  0000 C CNN
F 2 "" H 7450 2500 50  0001 C CNN
F 3 "" H 7450 2500 50  0001 C CNN
	1    7450 2500
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR0104
U 1 1 5E404473
P 7450 1150
F 0 "#PWR0104" H 7450 1000 50  0001 C CNN
F 1 "+3.3V" H 7465 1323 50  0000 C CNN
F 2 "" H 7450 1150 50  0001 C CNN
F 3 "" H 7450 1150 50  0001 C CNN
	1    7450 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C23
U 1 1 5E411C52
P 8050 1200
F 0 "C23" H 8165 1246 50  0000 L CNN
F 1 "1u" H 8165 1155 50  0000 L CNN
F 2 "" H 8088 1050 50  0001 C CNN
F 3 "~" H 8050 1200 50  0001 C CNN
	1    8050 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1150 7950 1150
Connection ~ 7450 1150
$Comp
L power:GND #PWR0105
U 1 1 5E412567
P 8050 1350
F 0 "#PWR0105" H 8050 1100 50  0001 C CNN
F 1 "GND" H 8055 1177 50  0000 C CNN
F 2 "" H 8050 1350 50  0001 C CNN
F 3 "" H 8050 1350 50  0001 C CNN
	1    8050 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1150 7950 1050
Wire Wire Line
	7950 1050 8050 1050
$EndSCHEMATC
