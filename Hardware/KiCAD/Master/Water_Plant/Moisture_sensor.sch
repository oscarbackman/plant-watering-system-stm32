EESchema Schematic File Version 4
LIBS:Water_Plant-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 6 7
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x04_Male J?
U 1 1 5E3DB6DE
P 5450 2950
AR Path="/5E3D345C/5E3DB6DE" Ref="J?"  Part="1" 
AR Path="/5E3DA444/5E3DB6DE" Ref="J4"  Part="1" 
F 0 "J4" H 5150 3400 50  0000 C CNN
F 1 "Conn_01x04_Male" H 5200 3300 50  0000 C CNN
F 2 "TerminalBlock_RND:TerminalBlock_RND_205-00289_1x04_P5.08mm_Horizontal" H 5450 2950 50  0001 C CNN
F 3 "~" H 5450 2950 50  0001 C CNN
	1    5450 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5800 3150 5700 3150
Wire Wire Line
	5400 1250 5800 1250
Wire Wire Line
	6650 1800 6650 2150
$Comp
L power:GND #PWR?
U 1 1 5E498B5E
P 6650 2150
AR Path="/5E3FCAC9/5E498B5E" Ref="#PWR?"  Part="1" 
AR Path="/5E3DA444/5E498B5E" Ref="#PWR0104"  Part="1" 
F 0 "#PWR0104" H 6650 1900 50  0001 C CNN
F 1 "GND" H 6655 1977 50  0000 C CNN
F 2 "" H 6650 2150 50  0001 C CNN
F 3 "" H 6650 2150 50  0001 C CNN
	1    6650 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 1250 6650 1250
Wire Wire Line
	7050 1250 6650 1250
Connection ~ 6650 1250
Wire Wire Line
	6650 1250 6650 1500
$Comp
L Transistor_FET:BSB280N15NZ3 Q?
U 1 1 5E498B4C
P 7250 1250
AR Path="/5E3FCAC9/5E498B4C" Ref="Q?"  Part="1" 
AR Path="/5E3DA444/5E498B4C" Ref="Q3"  Part="1" 
F 0 "Q3" H 7454 1296 50  0000 L CNN
F 1 "IRFR014PBF" H 7454 1205 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:TO-252-2" H 7250 1250 50  0001 C CIN
F 3 "https://www.elfa.se/Web/Downloads/14/_e/hyIRFR-IRFU014_E.pdf?pid=17117047" H 7250 1250 50  0001 L CNN
	1    7250 1250
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 5E498B46
P 5950 1250
AR Path="/5E3FCAC9/5E498B46" Ref="R?"  Part="1" 
AR Path="/5E3DA444/5E498B46" Ref="R26"  Part="1" 
F 0 "R26" V 6157 1250 50  0000 C CNN
F 1 "1k" V 6066 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 5880 1250 50  0001 C CNN
F 3 "~" H 5950 1250 50  0001 C CNN
	1    5950 1250
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E498B40
P 6650 1650
AR Path="/5E3FCAC9/5E498B40" Ref="R?"  Part="1" 
AR Path="/5E3DA444/5E498B40" Ref="R27"  Part="1" 
F 0 "R27" H 6720 1696 50  0000 L CNN
F 1 "10k" H 6720 1605 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6580 1650 50  0001 C CNN
F 3 "~" H 6650 1650 50  0001 C CNN
	1    6650 1650
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR049
U 1 1 5E400287
P 7350 900
F 0 "#PWR049" H 7350 750 50  0001 C CNN
F 1 "+3.3V" H 7500 950 50  0000 C CNN
F 2 "" H 7350 900 50  0001 C CNN
F 3 "" H 7350 900 50  0001 C CNN
	1    7350 900 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5E4AEC1A
P 5800 2850
F 0 "#PWR0105" H 5800 2600 50  0001 C CNN
F 1 "GND" H 5805 2677 50  0000 C CNN
F 2 "" H 5800 2850 50  0001 C CNN
F 3 "" H 5800 2850 50  0001 C CNN
	1    5800 2850
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5650 2850 5800 2850
Wire Wire Line
	5650 3050 5800 3050
Wire Wire Line
	5650 2950 7350 2950
$Comp
L Connector:TestPoint TP?
U 1 1 5E4248A8
P 6650 1250
AR Path="/5E4248A8" Ref="TP?"  Part="1" 
AR Path="/5E3DA444/5E4248A8" Ref="M_T1"  Part="1" 
F 0 "M_T1" V 6845 1322 50  0000 C CNN
F 1 "TestPoint" V 6754 1322 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 6850 1250 50  0001 C CNN
F 3 "~" H 6850 1250 50  0001 C CNN
	1    6650 1250
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP?
U 1 1 5E425421
P 5700 3200
AR Path="/5E425421" Ref="TP?"  Part="1" 
AR Path="/5E3DA444/5E425421" Ref="MoistLevel1"  Part="1" 
F 0 "MoistLevel1" V 5895 3272 50  0000 C CNN
F 1 "TestPoint" V 5804 3272 50  0000 C CNN
F 2 "TestPoint:TestPoint_Pad_1.0x1.0mm" H 5900 3200 50  0001 C CNN
F 3 "~" H 5900 3200 50  0001 C CNN
	1    5700 3200
	-1   0    0    1   
$EndComp
Text GLabel 5400 1250 0    49   Input ~ 0
Moister_sens_onoff
Text GLabel 5800 3150 2    50   Output ~ 0
MOISTURE_LEVEL
Wire Wire Line
	5700 3150 5700 3200
Connection ~ 5700 3150
Wire Wire Line
	5700 3150 5650 3150
Wire Wire Line
	7350 900  7350 1050
Wire Wire Line
	7350 1450 7350 2950
NoConn ~ 5800 3050
$EndSCHEMATC
