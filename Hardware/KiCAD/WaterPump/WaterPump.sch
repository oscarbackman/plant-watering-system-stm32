EESchema Schematic File Version 4
LIBS:WaterPump-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 5E3B0867
P 3750 3150
F 0 "R2" H 3820 3196 50  0000 L CNN
F 1 "10k" H 3820 3105 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 3680 3150 50  0001 C CNN
F 3 "~" H 3750 3150 50  0001 C CNN
	1    3750 3150
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5E3B1679
P 3050 2750
F 0 "R1" V 3257 2750 50  0000 C CNN
F 1 "1k" V 3166 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 2980 2750 50  0001 C CNN
F 3 "~" H 3050 2750 50  0001 C CNN
	1    3050 2750
	0    -1   -1   0   
$EndComp
$Comp
L Device:D_Schottky D1
U 1 1 5E3B5D2C
P 4000 2050
F 0 "D1" V 3954 2129 50  0000 L CNN
F 1 "D_Schottky" V 4045 2129 50  0000 L CNN
F 2 "Diode_SMD:D_3220_8050Metric_Pad2.65x5.15mm_HandSolder" H 4000 2050 50  0001 C CNN
F 3 "~" H 4000 2050 50  0001 C CNN
	1    4000 2050
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x02_Female J1
U 1 1 5E3B666A
P 4650 2000
F 0 "J1" H 4678 1976 50  0000 L CNN
F 1 "Conn_01x02_Female" H 4678 1885 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 4650 2000 50  0001 C CNN
F 3 "~" H 4650 2000 50  0001 C CNN
	1    4650 2000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 1900 4450 1900
Connection ~ 4450 1900
Wire Wire Line
	4450 1900 4450 2000
Wire Wire Line
	4000 2200 4000 2400
Wire Wire Line
	4000 2400 4450 2400
Wire Wire Line
	4450 2400 4450 2100
$Comp
L Transistor_FET:BSB280N15NZ3 Q1
U 1 1 5E3C3D30
P 4350 2750
F 0 "Q1" H 4554 2796 50  0000 L CNN
F 1 "NTF3055-100T1G" H 4554 2705 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 4350 2750 50  0001 C CIN
F 3 "https://docs.rs-online.com/e667/0900766b81276b14.pdf" H 4350 2750 50  0001 L CNN
	1    4350 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2750 3750 3000
Connection ~ 3750 2750
Wire Wire Line
	4450 2550 4450 2400
Connection ~ 4450 2400
$Comp
L Device:C C1
U 1 1 5E3C9FB5
P 4850 1500
F 0 "C1" V 5102 1500 50  0000 C CNN
F 1 "4.7u" V 5011 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4888 1350 50  0001 C CNN
F 3 "~" H 4850 1500 50  0001 C CNN
	1    4850 1500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4450 1500 4700 1500
Connection ~ 4450 1500
Wire Wire Line
	4450 1500 4450 1900
Wire Wire Line
	5000 1500 5250 1500
Wire Wire Line
	4150 2750 3750 2750
Text GLabel 3750 3600 3    50   Input ~ 0
GND
Wire Wire Line
	4450 2950 4450 3600
Wire Wire Line
	3750 3300 3750 3600
Text GLabel 5250 1700 3    50   Input ~ 0
GND
Wire Wire Line
	5250 1500 5250 1700
Text GLabel 2600 2750 0    50   Input ~ 0
MCU
Wire Wire Line
	2900 2750 2600 2750
Wire Wire Line
	3200 2750 3750 2750
Text GLabel 4450 1200 1    50   Input ~ 0
+12V
Wire Wire Line
	4450 1200 4450 1500
$Comp
L power:GND #PWR?
U 1 1 5E3D4717
P 4450 3600
F 0 "#PWR?" H 4450 3350 50  0001 C CNN
F 1 "GND" H 4455 3427 50  0000 C CNN
F 2 "" H 4450 3600 50  0001 C CNN
F 3 "" H 4450 3600 50  0001 C CNN
	1    4450 3600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
