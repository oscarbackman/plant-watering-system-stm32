EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Notes Line
	10650 5550 6950 5550
Wire Notes Line
	6950 5550 6950 4200
Wire Notes Line
	6950 4200 10650 4200
Wire Notes Line
	10650 4200 10650 5550
Text Notes 9100 5400 0    118  ~ 0
Sensor interface
$Comp
L Connector:Conn_01x04_Male J1
U 1 1 5E3C4189
P 9550 4850
F 0 "J1" H 9658 5131 50  0000 C CNN
F 1 "Conn_01x04_Male" H 9658 5040 50  0000 C CNN
F 2 "" H 9550 4850 50  0001 C CNN
F 3 "~" H 9550 4850 50  0001 C CNN
	1    9550 4850
	1    0    0    -1  
$EndComp
Text Label 9900 5050 0    50   ~ 0
SIG
Wire Wire Line
	9900 5050 9750 5050
Text Label 9900 4950 0    50   ~ 0
NC
Text Label 9900 4850 0    50   ~ 0
VCC
Text Label 9900 4750 0    50   ~ 0
GND
Wire Wire Line
	9900 4750 9750 4750
Wire Wire Line
	9900 4850 9750 4850
Wire Wire Line
	9900 4950 9750 4950
Wire Notes Line
	5650 3350 11150 3350
Wire Notes Line
	11150 3350 11150 550 
Wire Notes Line
	11150 550  5650 550 
Text Notes 10200 3250 0    118  ~ 0
CAN/ADC
$Comp
L Analog_ADC:MCP3002 U1
U 1 1 5E3C604A
P 7450 1850
F 0 "U1" H 7550 1169 50  0000 C CNN
F 1 "MCP3002" H 7550 1260 50  0000 C CNN
F 2 "" H 7450 1750 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21294E.pdf" H 7450 2050 50  0001 C CNN
	1    7450 1850
	-1   0    0    1   
$EndComp
Text Label 8150 1650 0    50   ~ 0
Analog_Input_1
Text Label 8150 2050 0    50   ~ 0
Analog_input_0
Wire Wire Line
	8150 2050 7950 2050
Wire Wire Line
	8150 1650 7950 1650
Text Label 6400 1750 2    50   ~ 0
Serial_Data_Output
Text Label 6400 1650 2    50   ~ 0
Serial_Data_Input
Wire Notes Line
	5650 550  5650 3350
Wire Wire Line
	6750 1650 6400 1650
Wire Wire Line
	6750 1750 6400 1750
Text Label 6400 1950 2    50   ~ 0
CLK
Wire Wire Line
	6750 1950 6400 1950
Text Label 6400 2050 2    50   ~ 0
Enable_Conversion
Wire Wire Line
	6750 2050 6400 2050
Text Label 7450 2500 2    50   ~ 0
GND
Wire Wire Line
	7450 2350 7450 2500
Text Label 7450 1150 0    50   ~ 0
VCC
Wire Wire Line
	7450 1350 7450 1150
Wire Notes Line
	6800 7650 6800 3950
Wire Notes Line
	6800 3950 550  3950
Wire Notes Line
	550  3950 550  7650
Wire Notes Line
	550  7650 6800 7650
$Comp
L MAX14827AATG_:MAX14827AATG+ U?
U 1 1 5E3CD411
P 3350 5850
F 0 "U?" H 3350 7120 50  0000 C CNN
F 1 "MAX14827AATG+" H 3350 7029 50  0000 C CNN
F 2 "QFN50P400X400X80-25N" H 3350 5850 50  0001 L BNN
F 3 "Maxim Integrated" H 3350 5850 50  0001 L BNN
F 4 "2/2 Transceiver IO-Link 24-TQFN _4x4_" H 3350 5850 50  0001 L BNN "Field4"
F 5 "MAX14827AATG+" H 3350 5850 50  0001 L BNN "Field5"
F 6 "TQFN-24 Maxim" H 3350 5850 50  0001 L BNN "Field6"
F 7 "None" H 3350 5850 50  0001 L BNN "Field7"
F 8 "Unavailable" H 3350 5850 50  0001 L BNN "Field8"
	1    3350 5850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
