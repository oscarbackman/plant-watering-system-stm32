Version 4
SymbolType BLOCK
LINE Normal 48 48 48 -32
LINE Normal 48 65 48 48
LINE Normal 128 16 48 65
LINE Normal 48 -32 128 16
WINDOW 0 58 -52 Bottom 2
WINDOW 3 62 8 Top 2
SYMATTR Value LM311
SYMATTR Prefix X
SYMATTR ModelFile C:\Users\Oscar\Downloads\slcj012\LM311.301
PIN 48 -16 LEFT 8
PINATTR PinName +
PINATTR SpiceOrder 1
PIN 48 48 LEFT 8
PINATTR PinName -
PINATTR SpiceOrder 2
PIN 96 -16 VLEFT 8
PINATTR PinName Vcc
PINATTR SpiceOrder 3
PIN 96 48 VRIGHT 8
PINATTR PinName -Vcc
PINATTR SpiceOrder 4
PIN 128 16 RIGHT 8
PINATTR PinName Out?
PINATTR SpiceOrder 5
PIN 64 48 VRIGHT 8
PINATTR PinName Gnd
PINATTR SpiceOrder 6
