clc
clear
Vdd = 3.3;

RF = 100;
R2 = 10000;  % High R2 and R3 ---> Better Rails, Issue: MCU might not react
R3 = 10000;

R23 = R2*R3/(R2+R3)
V23  = R3/(R2+R3)*Vdd

VOH = 3.0;
VOL = 0.3;
VTHL = VOH*(R23/(R23+RF)) + V23*(RF/(R23+RF))
VTLH = VOL*(R23/(R23+RF)) + V23*(RF/(R23+RF)) 
