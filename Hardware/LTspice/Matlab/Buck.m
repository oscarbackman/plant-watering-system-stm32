% choose R2 <20k
% Choose Freq (2.2Mhz) --> Look tabular 1 choose R4
% Decide Temp rise choose worst case Freq (ton) 1Mhz --> Tabular 1 choose RT_max)
% Calc R5
% Calc Vin_ps, larger than zeener!
% 
clc

Vout = 3.3;
Vin = 12;
R2 = 10000;
R1 = R2*(Vout/0.78-1)

R4 = 20000;    
f_sw = 2.2e6;   

%R4 = 54.9e3;    
%f_sw = 1e6;   %Choose Freq from tabular 1. 

ton_min = 100e-9  %Temp worst case or more likely "Typical Minimum On Time", take 75* --> 100e-9;

Vd = 0.4;
Vsw = 0.4;

DC = (Vout + Vd)/(Vin - Vsw + Vd)

DC_max = 1-f_sw/8.33

Vin_min = (Vout+Vd)/DC_max-Vd+Vsw
DC_min = f_sw*ton_min

Vin_ps = (Vout+Vd)/(DC_min)-Vd+Vsw % Breakdown voltage of zeener should be BELOW this value!

Vin_max = 20;
f_sw_min = (Vout+Vd)/(ton_min*(Vin_max+Vd-Vsw))
Rt_max = 26.5e3; % from f_sw_min! at 1.85Mhz
R5 = 2*(Vin_max-0.7)/(1/R4-1/Rt_max)
